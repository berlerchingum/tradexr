#ifndef _API_CONSUMER_h
#define _API_CONSUMER_h

#include "FreeRTOS.h"
#include "timers.h"

void xPotentiometerTimer(TimerHandle_t xTimer);

#endif
