#include "driver/gpio.h"
#include "config.h"

void configInit() {    

    gpio_config_t pin1;
    gpio_config_t pin2;
    gpio_config_t pin3;
    gpio_config_t pin4;
    gpio_config_t pin5;

    /* pin for start button */
    pin1.mode = GPIO_MODE_INPUT;
    pin1.pin_bit_mask = GPIO_Pin_13;
    pin1.pull_down_en = 0;
    pin1.pull_up_en = 1;
    gpio_config(&pin1);


    /* pin for the green led */
    pin2.mode = GPIO_MODE_OUTPUT;
    pin2.pin_bit_mask = GPIO_Pin_12;
    pin2.pull_down_en = 0;
    pin2.pull_up_en = 0;
    gpio_config(&pin2);

    /* pin for red led  */
    pin3.mode = GPIO_MODE_OUTPUT;
    pin3.pin_bit_mask = GPIO_Pin_14;
    pin3.pull_down_en = 0;
    pin3.pull_up_en = 0;
    gpio_config(&pin3);

    /* pin for input lcd */
    pin4.mode = GPIO_MODE_INPUT;
    pin4.pin_bit_mask = GPIO_Pin_5;
    pin4.pull_down_en = 0;
    pin4.pull_up_en = 0;
    gpio_config(&pin5);

    /* pin for input lcd */
    pin4.mode = GPIO_MODE_INPUT;
    pin4.pin_bit_mask = GPIO_Pin_4;
    pin4.pull_down_en = 0;
    pin4.pull_up_en = 0;
    gpio_config(&pin4);

    /* pin for output lcd */
    pin5.mode = GPIO_MODE_OUTPUT;
    pin5.pin_bit_mask = GPIO_Pin_5;
    pin5.pull_down_en = 0;
    pin5.pull_up_en = 0;
    gpio_config(&pin5);
}

void tasksInit(QueueHandle_t q) {
}

void timersInit(QueueHandle_t q) {
    TimerHandle_t xAutoReloadTimer;
    BaseType_t xTimerPotentiometer;
    xAutoReloadTimer = xTimerCreate("name", pdMS_TO_TICKS(500), pdTRUE, "potentiometer_timer", xPotentiometerTimer);

    if (xAutoReloadTimer != NULL) {
        xTimerPotentiometer = xTimerStart(xAutoReloadTimer, q);

        if (xTimerPotentiometer == pdPASS) {
            vTaskStartScheduler();
        }
    }
}
