#ifndef _config_h
#define _config_h

#include "FreeRTOS.h"
#include "timers.h"
#include "../potentiometer/potentiometer.h"
#include "freertos/queue.h"

void configInit();
void tasksInit(QueueHandle_t q);
void timersInit(QueueHandle_t q);

#endif
