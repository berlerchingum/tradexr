#ifndef _API_CONSUMER_h
#define _API_CONSUMER_h

struct Message {
    char messageId;
    char data[20];
};

void vApiTask( void *vParameters );

#endif
