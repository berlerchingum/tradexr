/*
   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <stdio.h>

#include "driver/gpio.h"
#include "esp_system.h"
#include "../../components/config/config.h"
#include "../../components/wifi/wifi.h"
#include "freertos/queue.h"

/******************************************************************************
 * FunctionName : app_main
 * Description  : entry of user application, init user function here
 * Parameters   : none
 * Returns      : none
*******************************************************************************/
void app_main(void)
{
    QueueHandle_t q = xQueueCreate(5, sizeof( uint32_t ));

    initWifi();
    timersInit(q);
    configInit();
    gpio_set_level(GPIO_NUM_12,1);
    gpio_set_level(GPIO_NUM_14,1);
}
